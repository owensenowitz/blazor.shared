﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

#nullable disable

namespace Blazor.Shared.Models
{
    public partial class Student
    {
        public int Id { get; set; }
        [Required]
        [MinLength(2, ErrorMessage = "UserName must contain at least 2 characters")]
        public string UserName { get; set; }
        [Required]
        public string Email { get; set; }
        public string PasswordHash { get; set; }
        public string PhoneNumber { get; set; }
        public string Major { get; set; }
        public string Class1 { get; set; }
        public string Class2 { get; set; }
        public string Class3 { get; set; }
        public string Class4 { get; set; }
        public string Class5 { get; set; }
        public string Class6 { get; set; }
    }
}
